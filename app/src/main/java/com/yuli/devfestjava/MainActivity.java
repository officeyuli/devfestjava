package com.yuli.devfestjava;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private TextView tvUserName;
    private EditText editUserName;
    private TextView tvUserPassword;
    private EditText editUserPassword;
    private TextView tvWarningText;
    private Button btnLogin;
    private Button btnClear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvUserName = findViewById(R.id.tvUserName);
        editUserName = findViewById(R.id.editUserName);
        tvUserPassword = findViewById(R.id.tvUserPassword);
        editUserPassword = findViewById(R.id.editUserPassword);
        tvWarningText = findViewById(R.id.tvWarningText);
        btnLogin = findViewById(R.id.btnLogin);
        btnClear = findViewById(R.id.btnClear);
    }


    private void initTextWatcher() {
        editUserName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    public void checkUserNameValid() {
//        callServer(binding.editUserName.getText().toString(), new Callback() {
//            public void onSuccess() {
//                binding.tvWarningText.setText("valid username");
//            }
//
//            public void onFail() {
//                binding.tvWarningText.setText("invalid username");
//            }
//        });
    }

    public void checkUserNameLength() {
        if (editUserName != null
                && editUserName.getText() != null
                && editUserName.getText().length() > 6) {
            if (tvWarningText != null) {
                tvWarningText.setText("User name too Long!");
            }
        }
    }

    public void clearWarningText() {
        if (tvWarningText != null) {
            tvWarningText.setText(null);
        }
    }
}